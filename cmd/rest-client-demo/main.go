package main

import (
	"flag"
	"log"

	"gitlab.com/public-go/examples/rest"
)

var startSimpleServer, demoClient bool

func main() {

	flag.BoolVar(&startSimpleServer, "sss", false, "When set a simple server will be started")
	flag.BoolVar(&demoClient, "dc", false, "When set a client will be demoed")

	flag.Parse()

	if demoClient {
		a := rest.QueryDomain("vanad.")
		log.Println("A total on vanad. :", a.TotalDomains)
		log.Println(a)

		b := rest.QueryDomainWithTLD("vanad", "com")
		log.Println(b)

		c := rest.QueryDomainWithTLD("google", "com")
		log.Println(c)
	}

	if startSimpleServer {
		rest.StartSimpleServer()
	}

	log.Println("the values: ", startSimpleServer, demoClient)
}
