package rest

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

// SomeResponse is an example
type SomeResponse struct {
	ID   int `json:"id"`
	Name string  `json:"name"`
}

func jsonResponder(w http.ResponseWriter, r *http.Request) {
	var resp SomeResponse
	resp = SomeResponse{40, "some name"}

	log.Println(resp)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	log.Println("test!")

	if json.NewEncoder(w).Encode(&resp) != nil {
		log.Fatal("unable to encode struct")
	}

}

func echoResponder(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "echo")
}

// StartSimpleServer starts a simple server
func StartSimpleServer() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		items := strings.Split(r.URL.Path, "/")

		fmt.Fprintf(w, "Hello, %s", strings.Join(items[1:], " "))
		log.Println("Request done: ", r.RequestURI)
	})

	http.HandleFunc("/echo", echoResponder)
	http.HandleFunc("/json", jsonResponder)

	log.Fatal(http.ListenAndServe(":8080", nil))

}
