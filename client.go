package rest

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

// DomainResponse contains the response of the api call
type DomainResponse struct {
	TotalDomains int `json:"total"`
	Time         int
	Domains      []string
}

// QueryDomain without tld
func QueryDomain(domain string) DomainResponse {
	return QueryDomainWithTLD(domain, "all")
}

// QueryDomainWithTLD with domain and tld known
func QueryDomainWithTLD(domain string, tld string) DomainResponse {

	if tld == "" {
		tld = "all"
	}

	req, err := http.NewRequest("GET", "https://api.domainsdb.info/search?query="+domain+"&tld="+tld, nil)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{
		Timeout: time.Second * 10,
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	var response DomainResponse

	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		log.Fatal(err)
	}

	return response

}
